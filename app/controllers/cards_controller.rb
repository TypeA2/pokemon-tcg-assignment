class CardsController < ApplicationController
  include ActionView::RecordIdentifier
  
  def index
    if params.has_key? :trainer_id
      @render_packs = true
      @pagy_packs, @packs = pagy(
        current_trainer.card_packs.order(:coins),
        page_param: :page_packs,
        items: params.fetch(:per_page, 8),
        link_extra: "data-turbo-action='advance'"
      )
    end
    
    collection = Card.
      with_image.
      with_card_set_id(params[:card_set_id]).
      with_name(params[:name]).
      with_rarity(params[:rarity]).
      with_owned_by(params[:trainer_id]).
      order(:name)

    @pagy_cards, @cards = pagy(
      collection,
      page_param: :page_cards,
      items: params.fetch(:per_page, 20),
      link_extra: "data-turbo-action='advance'"
    )
  end

  def create
    params.require(:id)
    params.require(:index)
    
    begin
      target_pack = current_trainer.card_packs.find params[:id]

      current_trainer.cards << target_pack.open

      # This is kind of cursed
      first_deleted = false
      current_trainer.card_packs = current_trainer.card_packs.select do |pack|
        if !first_deleted && pack.id == target_pack.id
          first_deleted = true
          false
        else
          true
        end
      end

    rescue ActiveRecord::RecordNotFound

    end

    redirect_back(fallback_location: root_path)
  end
end
