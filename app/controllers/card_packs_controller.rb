class CardPacksController < ApplicationController
  def index
    @pagy, @card_packs = pagy(
      CardPack.with_name(params[:name]).order(:name),
      items: params.fetch(:per_page, 3),
      link_extra: "data-turbo-action='advance'"
    )
  end

  def create
    target_pack = CardPack.find_by(id: params[:id])

    if current_trainer.coins >= target_pack.coins
      current_trainer.coins -= target_pack.coins
      new_coins = current_trainer.coins

      current_trainer.card_packs << target_pack

      current_trainer.save

      respond_to do |format|
        format.turbo_stream { render turbo_stream: [
          turbo_stream.update("coin_count", new_coins)
        ]}
      end
    else
      respond_to do |format|
        format.turbo_stream { render turbo_stream: turbo_stream.update("message", "Insufficient coins")}
      end
    end

  end
end
