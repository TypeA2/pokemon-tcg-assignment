import { Controller } from "@hotwired/stimulus"

export default class extends Controller {

  connect() {
    document.documentElement.addEventListener("turbo:submit-start", _ => {
      document.getElementById("message").innerText = "";
    });

    document.documentElement.addEventListener("turbo:before-stream-render", e => {
      if (e.detail.newStream.action === "update" && e.detail.newStream.target === "coin_count") {
        const new_count = parseInt(e.detail.newStream.children[0].innerHTML);

        for (const btn of document.getElementsByClassName("buy-btn")) {
          if (parseInt(btn.dataset.value) > new_count) {
            btn.setAttribute("disabled", "disabled");
          }
        }
      }
    });
  }
}
