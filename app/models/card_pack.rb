class CardPack < ApplicationRecord
  scope :with_name, ->(name) do
    where("LOWER(name) LIKE :name", name: "%#{name.downcase}%") if name.present?
  end

  # Open a card pack, return an array of all cards obtained
  def open
    # odds of common, uncommon, rare, secret rare, ultra rare in that order
    odds = case name.downcase
    when "basic"
      [0.5, 0.3, 0.14, 0.05, 0.01]
    when "premium"
      [0.2, 0.45, 0.25, 0.08, 0.02]
    end

    # This could be precomputed, but makes entering odds easier
    sum = 0
    odds_sum = odds.map do |x|
      prev = sum
      sum += x
      prev
    end

    results = []
    for i in 1..10 do
      roll = rand

      # Walk the exclusive prefix sum from back to front
      # This ensures we always have 1 result, regardless of outcome
      # since the first element is always 0
      for j in 1..5 do
        if roll >= odds_sum[5 - j]
          results.append 5 - j
          break
        end
      end
    end

    uncommon_plus_target = case name.downcase
    when "basic"
      2
    when "premium"
      5
    end

    uncommon_plus = results.count{|x| x >= 1}
    if uncommon_plus < uncommon_plus_target
      fix_count = uncommon_plus_target - uncommon_plus

      logger.debug "upgrading #{fix_count} cards"

      # Upgrade the required number of cards
      for i in 1..results.length do
        if results[i - 1] < 1
          results[i - 1] = 1
          fix_count -= 1
          
          break if fix_count == 0
        end
      end
    end

    if name.downcase == "premium"
      rare_plus = results.count{|x| x >= 2}
      fix_count = 2 - rare_plus

      # Simply upgrade from the start
      for i in 1..results.length do
        if results[i - 1] < 2
          results[i - 1] = 2
          fix_count -= 1

          break if fix_count == 0
        end
      end
    end

    # Results contains integers in [0, 4] mapping to rarities
    tiers = ["Common", "Uncommon", "Rare", "Ultra Rare", "None"]
    results.map do |tier|
      Card.where(rarity: tiers[tier]).sample
    end
  end

  has_and_belongs_to_many :trainers
end
